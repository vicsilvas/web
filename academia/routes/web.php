<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('professor','ProfessorController');
Route::get('/professor','ProfessorController@index')->name('professor');

Route::resource('aluno','AlunoController');
Route::get('/aluno','AlunoController@index')->name('aluno');

Route::resource('programa','ProgramaController');
Route::get('/programa','ProgramaController@index')->name('programa');

Route::resource('desempenho','DesempenhoController');
Route::get('/desempenho','DesempenhoController@index')->name('desempenho');