<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

public function store(Request $request){
	$request->validate([
		"nome"=>"required|max:255",
		"curso"=>"required|numeric",
		"descricao"=>"required",
		"arq"=>"required"
	]);
	$aluno = new Aluno([
		'nome' => $request->get('nome'),
		'curso_id' => $request->get('curso'),
		'descricao' => $request->get('descricao'),
		'arq' => $request->get('arq')
	]);
	$aluno->save();
	return redirect('/aluno')->with('success', 'Aluno inserido com sucesso!');
}
