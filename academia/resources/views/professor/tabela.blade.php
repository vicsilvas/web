
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br/>
        @endif
            <button type="button" class="btn btn-primary" onclick="window.location.href='{{action('ProfessorController@create')}}';">
                <i class="fa fa-plus-circle"></i> Adicionar
            </button>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Editar</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($professores as $professor)
                    <tr>
                        <td>{{($professor->nome)}}</td>
                        <td>{{($professor->email)}}</td>
                        <td>
                            <a class="btn btn-primary" href="{{route('professor.edit', $professor->id)}}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                        <td>
                            <form action="{{route('professor.destroy', $professor->id)}}" method="post" onSubmit="if(!confirm('Tem certeza?')){return false;}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
